
Welcome to jaiku.module.  This is an early prototype of a module
designed to send updates to Jaiku streams.  It is under active development.

FEATURES
--------
* Sends update to Jaiku stream whenever an enabled account posts a new node.


INSTALL REQUIREMENTS
--------------------
1.  Enable module from admin interface.
2.  For each user who wishes to update their Jaiku streams, they need to visit
    their account and enter their Jaiku username and Jaiku API key.
    The Jaiku API key can be obtained by logging into Jaiku in the normal way,
    and then visiting http://api.jaiku.com/.

BUGS / TO DO
------------
* No known bugs -- yet.  :-)
* Lots of things to do.

CONTRIBUTORS
------------
* Chris Johnson (#drupal:chrisxj, http://drupal.org/user/2794).
* Thanks to James Walker for his Twitter module, which provided the idea and outline.
